# GMDScores: automatic drum transcription of the Groove MIDI dataset

This repository provides the results of
the transcription of xxxx files 
selected from the [Groove MIDI dataset v.1.0.0](https://magenta.tensorflow.org/datasets/groove) 
with qparselib (todo lien qparse)

The transcription uses MIDI files from the dataset,
recorded by drummers on a electronic drumkit, 
the [Roland TD-11](https://www.roland.com/global/products/td-11k/).
Each element of the drumkit is mapped
to MIDI number, then mapped to a pitch
depending on the notation style (US or Agostini).
The mapping is as follows (from https://magenta.tensorflow.org/datasets/groove#drum-mapping):


| MIDI | note Agostini | note US | Roland        | GM               | DrumCode      |
|------|---------------|---------|---------------|------------------|---------------|
| 36   | D4            | F4      | Kick          | Bass             | `BD`          |
| 38   | B4            | C5      | Snare Head    | Ac. Snare        | `SN`          |
| 40   | B4            | C5      | Snare Rim     | El. Snare        | `SN`          |
| 37   | B4◇           | C5x     | Snare X-Stick | Side Stick       | `CS`          |
| 48   | E5            | E5      | Tom 1         | Hi-Mid Tom       | `TOMH`        |
| 50   | E5            | D5      | Tom 1 Rim     | Hi Tom           | `TOMH`        |
| 45   | D5            | B4      | Tom 2         | Low Tom          | `TOMM`        |
| 47   | D5            | A4      | Tom 2 Rim     | Low-Mid Tom      | `TOMM`        |
| 43   | F4            | G4      | Tom 3 Head    | High Floor Tom   | `TOMFL`       |
| 58   | F4            |         | Tom 3 Rim     | Vibraslap        | `TOMFL`       |
| 46   | B3ⓧ          | F5ⓧ    | HH Open Bow    | Open HH          | `HHO`         |
| 26   | B3ⓧ          |         | HH Open Edge  | N/A               | `HHO`         |
| 42   | B3x           | F5x     | HH Closed Bow | Closed HH        | `HHC`         |
| 22   | B3x           | D4ⓧ?   | HH Closed Edge | N/A              | `HHC`         |
| 44   | G5ⓧ           | D4x    | HH Pedal       | id.              | `HHP`         |
| 49   |               | A5x     | Crash 1 Bow    | Crash Cymbal 1  |  |
| 55   | B5x           | B5◇     | Crash 1 Edge   | Splash Cymbal   | `CR`          |
| 57   |               |         | Crash 2 Bow    | Crash Cymbal 2  |               |
| 52   |               | B5ⓧ     | Crash 2 Edge   | Chinese Cymbal |               |
| 51   | G5x           | G5x     | Ride Bow       | Ride Cymbal 1   | `RD`          |
| 59   | B5x           |         | Ride Edge      | Ride Cymbal 2   | `RC`          |
| 53   | B5◇           | G5◆     | Ride Bell      | Ride Bell       | `RB`          |



- HH Pedal = closed Hi-Hat
- HH Open bow = stick on the part between the edge and the bell – the largest surface. 

## Parsing times

| {} | Style      | Tempo | Parsing time | Number of measures |
|----|------------|-------|--------------|--------------------|
| 0  | rock       | 200   | 15.613981    | 261                |
| 1  | rock       | 125   | 5.983408     | 90                 |
| 2  | funk       | 80    | 2.647338     | 30                 |
| 3  | jazz       | 96    | 2.125434     | 24                 |
| 4  | jazz       | 116   | 6.363183     | 74                 |
| 5  | jazz       | 116   | 3.406357     | 38                 |
| 6  | funk       | 80    | 3.718246     | 41                 |
| 7  | reggae     | 126   | 6.844609     | 97                 |
| 8  | afrocuban  | 105   | 7.617157     | 98                 |
| 9  | latin      | 95    | 8.054993     | 85                 |
| 10 | reggae     | 78    | 2.948304     | 38                 |
| 11 | dance      | 120   | 9.603008     | 104                |
| 12 | soul       | 96    | 5.348123     | 53                 |
| 13 | latin      | 116   | 10.877218    | 110                |
| 14 | rock       | 80    | 1.529886     | 14                 |
| 15 | dance      | 170   | 6.328870     | 77                 |
| 16 | funk       | 80    | 3.186288     | 29                 |
| 17 | rock       | 140   | 8.318000     | 127                |
| 18 | jazz       | 215   | 2.990475     | 41                 |
| 19 | latin      | 116   | 2.754888     | 28                 |
| 20 | funk       | 125   | 7.301296     | 72                 |
| 21 | jazz       | 102   | 7.095456     | 79                 |
| 22 | neworleans | 84    | 5.323569     | 49                 |
| 23 | blues      | 134   | 0.661252     | 4                  |
| 24 | pop        | 132   | 1.772249     | 26                 |
<!-- | 25 | rock       | 200   | 18.895815    | 261                |
| 26 | rock       | 125   | 7.266766     | 90                 |
| 27 | funk       | 80    | 3.010071     | 30                 |
| 28 | jazz       | 96    | 2.442602     | 24                 |
| 29 | jazz       | 116   | 7.502516     | 74                 |
| 30 | jazz       | 116   | 3.945939     | 38                 |
| 31 | funk       | 80    | 4.305442     | 41                 |
| 32 | reggae     | 126   | 8.104025     | 97                 |
| 33 | afrocuban  | 105   | 8.829274     | 98                 |
| 34 | latin      | 95    | 8.832522     | 85                 |
| 35 | reggae     | 78    | 3.351463     | 38                 |
| 36 | dance      | 120   | 10.039703    | 104                |
| 37 | soul       | 96    | 5.091306     | 53                 |
| 38 | latin      | 116   | 11.448567    | 110                |
| 39 | rock       | 80    | 1.612696     | 14                 |
| 40 | dance      | 170   | 5.776824     | 77                 |
| 41 | funk       | 80    | 2.889653     | 29                 |
| 42 | rock       | 140   | 7.728024     | 127                |
| 43 | jazz       | 215   | 3.011334     | 41                 |
| 44 | latin      | 116   | 2.879286     | 28                 |
| 45 | funk       | 125   | 7.308483     | 72                 |
| 46 | jazz       | 102   | 6.574776     | 79                 |
| 47 | neworleans | 84    | 5.142282     | 49                 |
| 48 | blues      | 134   | 0.623374     | 4                  |
| 49 | pop        | 132   | 1.748757     | 26                 | -->


___

## files 

### directory `DiSj_n` 
it corresponds to the GMD entry of:  

- drummer `i`  
- session `j`  
- MIDI file `n`  

**MIDI file names**:  

```
n_genre_tempo_(beat | fill)_ts
```

- `n` is the `n` in directory name
- genre: for example rock, funk...
- tempo: in bpm
- `(beat | fill)` = `beat` or `fill`
- `ts` = time signature, for example `4-4`


### directory `schemas`

### file `param.ini`

<!-- ### file `mapping.md`
info on the MIDI keys mapping for the 
 MIDI drum kit 
used to record the MIDI performances in GMD.
It uses some pitch values that differ from the General MIDI (GM) Specifications. 

see
https://magenta.tensorflow.org/datasets/groove#drum-mapping -->

<!-- ### metadata 
txt files of GMD `info.cvs`



## beat extraction
with beatroot Sonic Visualizer plugin  
https://code.soundsoftware.ac.uk/projects/beatroot  

extraction on audio files of GMD (synthesized)

file name = GMD file - `.wav` + `_beatroot.txt`

attributes:

- file:  audio file (in GMD) processed by the plugin
- tpb:   number of ticks per beat
- bpm:   number of beats per measure
- up:    number of ticks in upbeat (i.e. first downbeat is up+1)
- tempo: in beat per minute, metronome when recording midi, 
         as given in GMD annotation file `info.cvs`
- dur:   in seconds, total duration
         as given in GMD annotation file `info.cvs`
 -->


